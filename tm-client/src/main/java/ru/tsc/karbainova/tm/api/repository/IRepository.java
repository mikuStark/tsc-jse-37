package ru.tsc.karbainova.tm.api.repository;


import ru.tsc.karbainova.tm.endpoint.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    void clear();

    void addAll(List<E> entities);

    void remove(E entity);

    E add(final E entity);


}
