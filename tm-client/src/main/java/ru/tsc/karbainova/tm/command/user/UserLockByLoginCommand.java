package ru.tsc.karbainova.tm.command.user;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.Session;

public class UserLockByLoginCommand extends AbstractCommand {
    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " Lock user";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        Session session = serviceLocator.getSession();
        serviceLocator.getAdminUserEndpoint().lockUserByLoginUser(session, login);
    }

}
