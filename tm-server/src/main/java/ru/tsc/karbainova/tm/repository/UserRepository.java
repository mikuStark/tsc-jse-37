package ru.tsc.karbainova.tm.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.FieldConst;
import ru.tsc.karbainova.tm.api.TableConst;
import ru.tsc.karbainova.tm.api.repository.IUserRepository;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NonNull final Connection connection) {
        super(connection);
    }

    protected String getTableName() {
        return TableConst.USER_TABLE;
    }

    @SneakyThrows
    @Override
    protected User fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NonNull final User user = new User();
        user.setFirstName(row.getString(FieldConst.FIRST_NAME));
        user.setLastName(row.getString(FieldConst.LAST_NAME));
        user.setMiddleName(row.getString(FieldConst.MIDDLE_NAME));
        user.setEmail(row.getString(FieldConst.EMAIL));
        user.setId(row.getString(FieldConst.ID));
        user.setLocked(Boolean.valueOf(row.getString(FieldConst.LOCKED)));
        user.setLogin(row.getString(FieldConst.LOGIN));
        user.setRole(Role.valueOf(row.getString(FieldConst.ROLE)));
        user.setPasswordHash(row.getString(FieldConst.PASSWORD_HASH));
        return user;
    }

    @Override
    @SneakyThrows
    public User add(User entity) {
        if (entity == null) return null;
        @NonNull final String query = "INSERT INTO " + getTableName() +
                "(id, first_name, last_name, middle_name, email, login, role, locked, password_hash)" +
                "VALUES(?,?,?,?,?,?,?,?,?)";
        @NonNull PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getFirstName());
        statement.setString(3, entity.getLastName());
        statement.setString(4, entity.getMiddleName());
        statement.setString(5, entity.getEmail());
        statement.setString(6, entity.getLogin());
        statement.setString(7, entity.getRole().toString());
        statement.setBoolean(8, entity.getLocked());
        statement.setString(9, entity.getPasswordHash());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    @SneakyThrows
    public User update(User entity) {
        if (entity == null) return null;
        @NonNull final String query = "UPDATE " + getTableName() +
                "SET firstName=?, lastName=?, middleName=?, email=?, login=?, locked=?, passwordHash=?)";
        @NonNull PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getFirstName());
        statement.setString(3, entity.getLastName());
        statement.setString(4, entity.getMiddleName());
        statement.setString(5, entity.getEmail());
        statement.setString(6, entity.getLogin());
        statement.setString(7, entity.getRole().toString());
        statement.setString(8, entity.getLocked().toString());
        statement.setString(9, entity.getPasswordHash());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    @SneakyThrows
    public User findByLogin(@NonNull final String login) {
        @NonNull final String query = "SELECT * FROM " + getTableName() + " WHERE login = ? LIMIT 1";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        @NonNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NonNull final User result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public User findByEmail(@NonNull final String email) {
        @NonNull final String query = "SELECT * FROM " + getTableName() + " WHERE email = ? LIMIT 1";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, email);
        @NonNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NonNull final User result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void removeUser(@NonNull final User user) {
        @NonNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@NonNull final String login) {
        @NonNull final String query = "DELETE FROM " + getTableName() + " WHERE login = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        statement.executeUpdate();
        statement.close();
    }
}
