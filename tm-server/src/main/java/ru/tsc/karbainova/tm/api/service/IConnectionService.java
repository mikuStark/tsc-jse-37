package ru.tsc.karbainova.tm.api.service;

import lombok.SneakyThrows;

import java.sql.Connection;

public interface IConnectionService {

    Connection getConnection();
}
