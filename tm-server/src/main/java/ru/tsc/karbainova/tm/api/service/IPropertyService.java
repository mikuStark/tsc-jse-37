package ru.tsc.karbainova.tm.api.service;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.ISaltSettings;
import ru.tsc.karbainova.tm.api.ISignatureSetting;

public interface IPropertyService extends ISaltSettings, ISignatureSetting {
    @NonNull String getJdbcUser();

    @NonNull String getJdbcPassword();

    @NonNull String getJdbcUrl();

    @NonNull String getApplicationVersion();

    @NonNull String getDeveloperName();

    @NonNull String getDeveloperEmail();

    @NonNull String getServerHost();

    @NonNull String getServerPort();
}
