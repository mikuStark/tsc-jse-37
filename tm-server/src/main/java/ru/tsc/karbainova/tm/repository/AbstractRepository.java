package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.IRepository;
import ru.tsc.karbainova.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NonNull
    protected final Connection connection;

    protected AbstractRepository(@NonNull Connection connection) {
        this.connection = connection;
    }

    protected abstract String getTableName();

    protected abstract E fetch(@Nullable final ResultSet row);

    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NonNull final String query = "SELECT * FROM " + getTableName();
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        @NonNull final ResultSet resultSet = statement.executeQuery();
        @NonNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public boolean exists(final String id) {
        @NonNull final String query = "SELECT * FROM " + getTableName() + "WHERE id = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        @NonNull final ResultSet resultSet = statement.executeQuery();
        return resultSet != null;
    }

    @Override
    public abstract E add(final E entity);

    public java.sql.Date prepare(final Date date) {
        if (date == null) return null;
        return (java.sql.Date) new Date(date.getTime());
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NonNull final Statement statement = connection.createStatement();
        @NonNull final String query = "DELETE * FROM " + getTableName();
        statement.executeUpdate(query);
        statement.close();
    }

    @Override
    @SneakyThrows
    public E findById(@Nullable final String id) {
        @NonNull final String query = "SELECT * FROM " + getTableName() + " WHERE id = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NonNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NonNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NonNull final String query = "DELETE * FROM " + getTableName() + " WHERE id = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeQuery();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void remove(final @NonNull E entity) {
        @NonNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.executeQuery();
        statement.close();
    }

    @Override
    public void addAll(Collection<E> collection) {
        if (collection == null) return;
        for (E i : collection) {
            add(i);
        }
    }
}
